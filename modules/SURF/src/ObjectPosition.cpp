/* 
 * File:   ObjectPosition.cpp
 * Author: Andoni Aguirrezabal
 * 
 */

#include "ObjectPosition.h"

ObjectPosition::ObjectPosition() :
    featureDetector(MIN_HESSIAN)
{
    minDist = 100;
    maxDist =   0;

    cam = new PointCloudCapture();
    imgObject = cv::imread("test.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    
    // Detect Keypoints and Extract Descriptors in source image
    featureDetector.detect(imgObject, objKeypoints);
    descriptionExtractor.compute(imgObject, objKeypoints, descObject);

    // Descriptor Matching
    double descMaxDist = 0, descMinDist = 100;
    cv::FlannBasedMatcher descMatcher;
    std::vector<cv::DMatch> descMatches;
    
    // Start Capturing Data
    cam->startCapture();
    
    // Get First Data Frame
    cam->getFrame(camData);
}

ObjectPosition::~ObjectPosition() {
}

void ObjectPosition::getObjectPosition(void) {
    // Create Keypoints and Descriptor for Scene
    std::vector<cv::KeyPoint> sceneKeypoints;
    cv::Mat descScene;

    // Create Matches
    std::vector<cv::DMatch> descMatches;

    std::cout << "Grabbing new frame\n";
    cam->getFrame(camData);

    // Detect Keypoints and Extract Descriptors in source image
    featureDetector.detect(camData.srcImg, sceneKeypoints);
    descriptionExtractor.compute(camData.srcImg, sceneKeypoints, descScene);

    // Match Item to Picture
    descMatcher.match(descObject, descScene, descMatches);

    for(int i = 0; i < descObject.rows; i++) {
        double dist = descMatches[i].distance;
        if(dist < minDist) {
            minDist = dist;
        }
        if(dist > maxDist) {
            maxDist = dist;
        }
    }

    // Obtain Good Matches?
    std::vector<cv::DMatch> descGoodMatches;
    for(int i = 0; i < descObject.rows; i++) {
        if(descMatches[i].distance < 3*minDist) {
            descGoodMatches.push_back(descMatches[i]);
        }
    }
    
    // Create Matches Test Image
    cv::Mat imgMatches;
    cv::drawMatches(imgObject, objKeypoints, camData.srcImg, sceneKeypoints,
                    descGoodMatches, imgMatches, cv::Scalar::all(-1), 
                    cv::Scalar::all(-1), std::vector<char>(), 
                    cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
    
    // Localize the object in the Scene
    std::vector<cv::Point2f> coordObj;
    std::vector<cv::Point2f> coordScene;
    
    // Get the keypoints from the good matches
    for(int i = 0; i < descGoodMatches.size(); i++) {
        coordObj.push_back(objKeypoints[descGoodMatches[i].queryIdx].pt);
        coordScene.push_back(sceneKeypoints[descGoodMatches[i].trainIdx].pt);
    }

    cv::Mat sceneHomog = cv::findHomography(coordObj, coordScene, CV_RANSAC);

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<cv::Point2f> objCorners(4);
    std::vector<cv::Point2f> sceneCorners(4);

    objCorners[0] = cvPoint(0,0); objCorners[1] = cvPoint(imgObject.cols, 0);
    objCorners[2] = cvPoint(imgObject.cols, imgObject.rows); 
    objCorners[3] = cvPoint(0, imgObject.rows);
    
    cv::perspectiveTransform(objCorners, sceneCorners, sceneHomog);
    
    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    cv::line(imgMatches, sceneCorners[0] + cv::Point2f(imgObject.cols, 0), 
                         sceneCorners[1] + cv::Point2f(imgObject.cols, 0), 
                         cv::Scalar(0, 255, 0), 4);
    cv::line(imgMatches, sceneCorners[1] + cv::Point2f(imgObject.cols, 0), 
                         sceneCorners[2] + cv::Point2f(imgObject.cols, 0), 
                         cv::Scalar(0, 255, 0), 4);
    cv::line(imgMatches, sceneCorners[2] + cv::Point2f(imgObject.cols, 0), 
                         sceneCorners[3] + cv::Point2f(imgObject.cols, 0),
                         cv::Scalar(0, 255, 0), 4);
    cv::line(imgMatches, sceneCorners[3] + cv::Point2f(imgObject.cols, 0),
                         sceneCorners[0] + cv::Point2f(imgObject.cols, 0), 
                         cv::Scalar(0, 255, 0), 4);
    
    cv::imshow("Test", imgMatches);
    cv::waitKey(1);
}
