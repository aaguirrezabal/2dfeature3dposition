/* 
 * File:   ObjectPosition.h
 * Author: andoni
 *
 * Created on June 4, 2015, 3:40 PM
 */

#ifndef OBJECTPOSITION_H
#define	OBJECTPOSITION_H

#define MIN_HESSIAN 400

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#include "PointCloudCapture/PointCloudCapture.h"

class ObjectPosition {
public:
    ObjectPosition();
    virtual ~ObjectPosition();

public:
    void getObjectPosition(void);

// Configuration
private:
    double minDist, maxDist;
    
// Data Input
private:
    PointCloudCapture* cam;
    InputData camData;

    // Local Image
    cv::Mat imgObject;

// SURF Components
private:
    // Keypoint Detector
    cv::SurfFeatureDetector featureDetector;
    std::vector<cv::KeyPoint> objKeypoints;
    
    // SURF Descriptor Extractors
    cv::SurfDescriptorExtractor descriptionExtractor;
    cv::Mat descObject;
    
    // Descriptor Matching
    cv::FlannBasedMatcher descMatcher;

};

#endif	/* OBJECTPOSITION_H */

