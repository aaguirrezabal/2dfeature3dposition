/* 
 * File:   ObjectPosition.cpp
 * Author: Andoni Aguirrezabal
 * 
 */

#include "ObjectPosition.h"

ObjectPosition::ObjectPosition() {
    cam = new PointCloudCapture();
    
    // Config HSV Threshold
    // Hue
    intLowH = 138;
    intHighH = 170;
    
    // Saturation
    intLowS = 102;
    intHighS = 255;
    
    // Velocity
    intLowV = 128;
    intHighV = 255;
    
    // Start Capturing Data
    cam->startCapture();
    
    // Get First Data Frame
    cam->getFrame(camData);
}

ObjectPosition::~ObjectPosition() {
}

void ObjectPosition::getObjectPosition(void) {
    cv::Mat camDataHSV;
    cv::Mat camDataThreshold;
    
    cam->getFrame(camData);
    
    // Convert Color to HSV
    cv::cvtColor(camData.srcImg, camDataHSV, cv::COLOR_BGR2HSV);
    
    cv::inRange(camDataHSV, 
                cv::Scalar(intLowH, intLowS, intLowV),
                cv::Scalar(intHighH, intHighS, intHighV),
                camDataThreshold);
    
    cv::imshow("Test", camDataThreshold);
    cv::waitKey(1);
}
