/* 
 * File:   ObjectPosition.h
 * Author: andoni
 *
 * Created on June 4, 2015, 3:40 PM
 */

#ifndef OBJECTPOSITION_H
#define	OBJECTPOSITION_H

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "PointCloudCapture/PointCloudCapture.h"

class ObjectPosition {
public:
    ObjectPosition();
    virtual ~ObjectPosition();

public:
    void getObjectPosition(void);

// Configuration
int intLowH, intHighH;
int intLowS, intHighS;
int intLowV, intHighV;
    
// Data Input
private:
    PointCloudCapture* cam;
    InputData camData;

};

#endif	/* OBJECTPOSITION_H */

