/* 
 * File:   ObjectPosition.cpp
 * Author: Andoni Aguirrezabal
 */

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <pcl/common/centroid.h>
#include <opencv2/highgui/highgui.hpp>

#include "ObjectPosition.h"
#include "Utils.hpp"

ObjectPosition::ObjectPosition() {
    cam = new PointCloudCapture();
    
    // Config HSV Threshold
    // Hue
    intLowH = 138;
    intHighH = 170;
    
    // Saturation
    intLowS = 102;
    intHighS = 255;
    
    // Velocity
    intLowV = 128;
    intHighV = 255;
    
    // Start Capturing Data
    cam->startCapture();
    
    // Get First Data Frame, set info
    cam->getFrame(camData);
    imgWidth = camData.srcImg.size().width;
    imgHeight = camData.srcImg.size().height;
}

ObjectPosition::~ObjectPosition() {
}

Eigen::Vector4f ObjectPosition::getCenter(int centerX, int centerY, int imgWidth, 
        int imgHeight, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr srcCloud) 
{
    int pixelWidth = 2;
    int minX = 0, maxX = 0, minY = 0, maxY = 0;
    
    pcl::PointXYZRGBA tempPos;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr tempCloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
    
    Eigen::Vector4f centroid;
    
    //minX
    if((centerX - pixelWidth) <= 0) {
        minX = 0;
    } else {
        minX = centerX - pixelWidth;
    }
    
    //maxX
    if((centerX + pixelWidth) >= imgWidth) {
        maxX = imgWidth;
    } else {
        maxX = centerX + pixelWidth;
    }
    
    //minY
    if((centerY - pixelWidth) <= 0) {
        minY = 0;
    } else {
        minY = centerY - pixelWidth;
    }
    
    //maxY
    if((centerY + pixelWidth) >= imgHeight) {
        maxY = imgHeight;
    } else {
        maxY = centerY + pixelWidth;
    }
    
    for(int i = minX; i <= maxX; i++) {
        for(int j = minY; j <= maxY; j++) {
            tempPos = srcCloud->points[j * srcCloud->width + i];
            if (std::isnan(tempPos.x) || std::isnan(tempPos.y)) {
                //PASS
            } else {
                tempCloud->points.push_back(tempPos);
            }
        }
    }

    pcl::compute3DCentroid(*tempCloud, centroid);
    return centroid;
}

Eigen::Matrix4f ObjectPosition::getCameraRotation(pcl::PointXYZ const& point) {
    float x = 0.0, y = 0.0, z = 0.0;
    Eigen::Matrix4f rotationMatrix = Eigen::Matrix4f::Zero();

    x = -1 * atan2(point.y, point.z);
    y = atan2(point.x, point.z);

#ifdef _DEBUG_INFO
    std::cout << "{X,Y,Z} = {" << point.x << ", " << point.y << ", " << point.z
              << "}\n";
    std::cout << "[X,Y,Z] = [" << rad2deg(x) << ", " << rad2deg(y) << ", "
              << rad2deg(z) << "]\n";
#endif
    
    rotationMatrix = (genRotX(x) * genRotY(y));
    return rotationMatrix;
}

pcl::PointXYZ ObjectPosition::updateObjectPosition(void) {
    cv::Mat camDataHSV;
    cv::Mat camDataThreshold;
    int posX, posY;
    
    cam->getFrame(camData);
    
    // Convert Color to HSV
    cv::cvtColor(camData.srcImg, camDataHSV, cv::COLOR_BGR2HSV);
    
    cv::inRange(camDataHSV, 
                cv::Scalar(intLowH, intLowS, intLowV),
                cv::Scalar(intHighH, intHighS, intHighV),
                camDataThreshold);
    
    // Morphological Opening (Remove small objects from the foreground)
    cv::erode(camDataThreshold, camDataThreshold, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(BLOBFILL_SIZE, BLOBFILL_SIZE)));
    cv::dilate(camDataThreshold, camDataThreshold, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(BLOBFILL_SIZE, BLOBFILL_SIZE))); 

    // Morphological Closing (Fill small holes in the foreground)
    // cv::dilate(camDataThreshold, camDataThreshold, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(BLOBFILL_SIZE, BLOBFILL_SIZE))); 
    // cv::erode(camDataThreshold, camDataThreshold, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(BLOBFILL_SIZE, BLOBFILL_SIZE)));

    // Calculate the blob moments
    cv::Moments blobMoments = cv::moments(camDataThreshold);
    
#ifdef _DEBUG_GUI
    cv::imshow("Camera Output", camDataThreshold);
    cv::waitKey(1);
#endif
    
    if (blobMoments.m00 > 1000) {
        posX = blobMoments.m10 / blobMoments.m00;
        posY = blobMoments.m01 / blobMoments.m00;
        lastPosition.getArray4fMap() = getCenter(posX, posY, imgWidth, imgHeight, 
                                                 camData.srcCloud);
    } else {
#ifdef _DEBUG_INFO
        std::cout << "Object not detected\n";
#endif
    }
    
    return lastPosition;
}
