/* 
 * File:   main.cpp
 * Author: Andoni Aguirrezabal (aaguirrezabal@live.com)
 */

#include <cstdlib>
#include <iostream>

#include "ObjectPosition.h"

using namespace std;

int main(int argc, char** argv) {
    ObjectPosition test;
    pcl::PointXYZ pos;
    Eigen::Matrix4f matrixTest = Eigen::Matrix4f::Zero();
    
    while(true) {
        pos = test.updateObjectPosition();
        matrixTest = test.getCameraRotation(pos);
        std::cout << matrixTest << "\n";
    }
    return 0;
}

