/* 
 * File:   Utils.hpp
 * Author: Andoni Aguirrezabal
 *
 */

#ifndef UTILS_HPP
#define	UTILS_HPP

#include <eigen3/Eigen/Core>

#define PI 3.14159

// Debugging Purposes
float rad2deg(float const& radians) {
    return (float)((radians*180)/(PI));
}

Eigen::Matrix4f genRotX(float const& theta) {
    Eigen::Matrix4f rotMatrix = Eigen::Matrix4f::Zero();

    float c = cos(theta);
    float s = sin(theta);
    rotMatrix << 1, 0,  0, 0,
                 0, c, -s, 0,
                 0, s,  c, 0,
                 0, 0,  0, 1;

    return rotMatrix;
}

Eigen::Matrix4f genRotY(float const& theta) {
    Eigen::Matrix4f rotMatrix = Eigen::Matrix4f::Zero();

    float c = cos(theta);
    float s = sin(theta);
    rotMatrix << c, -s, 0, 0,
                 s,  c, 0, 0,
                 0,  0, 1, 0,
                 0,  0, 0, 1;

    return rotMatrix;
}

#endif	/* UTILS_HPP */

