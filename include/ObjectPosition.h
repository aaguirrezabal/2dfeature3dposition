/* 
 * File:   ObjectPosition.h
 * Author: Andoni Aguirrezabal
 */

#ifndef OBJECTPOSITION_H
#define	OBJECTPOSITION_H

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "PointCloudCapture/PointCloudCapture.h"

#define BLOBFILL_SIZE 3

class ObjectPosition {
public:
    ObjectPosition();
    virtual ~ObjectPosition();

public:
    pcl::PointXYZ updateObjectPosition(void);
    Eigen::Matrix4f getCameraRotation(pcl::PointXYZ const& point);

private:
    Eigen::Vector4f getCenter(int centerX, int centerY, int imgWidth, int imgHeight,
                              pcl::PointCloud<pcl::PointXYZRGBA>::Ptr srcCloud);

// Configuration
private:
    int imgWidth, imgHeight;
    int intLowH, intHighH;
    int intLowS, intHighS;
    int intLowV, intHighV;

// Data Input
private:
    PointCloudCapture* cam;
    InputData camData;

// Calculated Position
private:
    pcl::PointXYZ lastPosition;
};

#endif	/* OBJECTPOSITION_H */

